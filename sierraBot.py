import logging
import os
import datetime
import sys

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from requests_html import HTMLSession
from selenium.webdriver.support.wait import WebDriverWait
from twocaptcha import TwoCaptcha
import traceback
import time
import re

from urllib3.exceptions import MaxRetryError

base_url = 'https://www.sierra.com/'
solver = TwoCaptcha('0da786922dbfe15d605291cc23f895c0')
TEST = False
SESSIONS_COUNT = 0
REQUESTS_DELAY = 0
DEFAULT_DRIVER_WAIT = 0


class Shipping:
    firstname = "Amy"
    lastName = "Quinn"
    address = "2996 Hartley Drive"
    zipCode = "37043"
    city = "Clarksville"
    state = "TN"
    phoneNumber = "9319198600"
    email = "amyquinn2021@gmail.com"


class Payment:
    creditcard = "4266902067069995"
    cvv = "732"
    experationMonth = "12"
    experationYear = "2023"


def print_t(text):
    current_time = datetime.datetime.now()
    time_str = current_time.strftime('[%d.%m.%g %T]')
    print(f'{time_str} {text}')
    logging.info(text)


def str_to_file_name(string):
    return string.replace(':', ' ').replace('-', ' ').replace('?', '').strip()


def print_progress_bar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', print_end="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filled_length = int(length * iteration // total)
    bar = fill * filled_length + '-' * (length - filled_length)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=print_end)
    # Print New Line on Complete
    if iteration == total:
        print()


def get_basic_driver(executable_path=None, headless=False):
    print_t('Starting webdriver...')
    options = webdriver.ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument("--remote-debugging-port=9222")
    options.add_argument('log-level=3')
    options.add_argument("--incognito")
    if headless:
        options.add_argument("--headless")
    options.add_experimental_option("excludeSwitches", ['enable-automation'])
    options.add_experimental_option('useAutomationExtension', False)
    if not TEST:
        prefs = {"profile.managed_default_content_settings.images": 2}
        options.add_experimental_option("prefs", prefs)
    if executable_path:
        _driver = webdriver.Chrome(options=options, executable_path=executable_path)
    else:
        _driver = webdriver.Chrome(options=options)

    _driver.execute_cdp_cmd("Network.enable", {})
    _driver.set_window_position(0, 0)
    _driver.set_window_size(1024, 900)
    print_t('Webdriver started.')
    return _driver


def set_data_by_id(driver, link, key):
    while True:
        try:
            driver.find_element_by_id(link).send_keys(key)
            break
        except:
            pass


def set_data_by_xpath(driver, xpath, value):
    driver.find_element_by_xpath(xpath).send_keys(value)


def find_click_button(driver, text):
    while True:
        try:
            btn = driver.find_element_by_id(text)
            btn.click()
            break
        except:
            pass


def get_product_links(target_name, empty=False):
    """
    Returns list of elements "items",
    each containing a link to product detail page
    """
    base_shop = base_url
    session = HTMLSession()
    if not empty:
        session.get(base_shop)
    item = target_name
    return item, session


def get_matched_and_available(target_name):
    """
    Given a target name, filter the product on main page,
    and return links to products with available items

    checked_urls: if already checked (and not a match in product name),
    skip in future checks

    Exactly how this should work, depends on how the drop works - is the page already there,
    just not for sale yet? Or page is added at drop time?
    """
    potential_urls = []
    sessions = []
    session_index = 0
    print_t('Initializing sessions...')
    for i in range(0, SESSIONS_COUNT):
        print_progress_bar(i, SESSIONS_COUNT, prefix=datetime.datetime.now().strftime('[%d.%m.%g %T]'),
                           suffix='Complete', length=45)
        try:
            item, session = get_product_links(target_name, empty=True)
            sessions.append(session)
        except MaxRetryError:
            print_t('MaxRetryError, sleeping for 10s...')
            time.sleep(10)
        time.sleep(0.01)

    print_progress_bar(SESSIONS_COUNT, SESSIONS_COUNT, prefix=datetime.datetime.now().strftime('[%d.%m.%g %T]'),
                       suffix='Complete', length=45)

    if base_url in item:
        target_url = item
    else:
        target_url = base_url + item
    print_t(target_url)
    r = sessions[0].get(target_url)
    found = True
    if found:
        item_available = False
        print_t("Checking for item: Click CTR + C to stop search.")
        while not item_available:
            item_available = check_can_buy(r)
            if not item_available:
                r = sessions[session_index].get(target_url)
                session_index += 1
                if session_index == SESSIONS_COUNT - 1:
                    session_index = 0
                time.sleep(REQUESTS_DELAY)
        potential_urls.append(target_url)
    else:
        print_t(f'Not a match')

    return potential_urls


def check_can_buy(r):
    """
    Given a page (returned by session.get(target_url)),
    find if there is such html code within:
    <input type="submit" name="commit" value="add to cart" class="button">
    Returns True if so, False if not
    """
    try:
        buy_btn = r.html.find('input[id="addToCartButton"]', first=True)
        buy_btn_parent = r.html.xpath('//input[@id="addToCartButton"]/..')[0]
        quantity_input = r.html.find('input[id="quantity"]', first=True)
    except IndexError:
        return False

    is_visible = True
    try:
        if 'display: none' in buy_btn_parent.attrs['style']:
            is_visible = False
    except KeyError:
        # no 'style' attribute, visible then
        pass

    return buy_btn is not None and is_visible and quantity_input is not None


def perform_purchase(url, product_quantity):
    """
    Given url of product, add to cart then checkout
    """
    shipping = Shipping()
    payment = Payment()
    driver = None
    try:
        if TEST:
            driver = get_basic_driver()
        else:
            driver = get_basic_driver(headless=True)

        is_added = False
        print_t('Adding to cart...')
        while not is_added:
            is_added = add_to_cart(driver, url, product_quantity)
            if not is_added:
                time.sleep(REQUESTS_DELAY)
        print_t('Added.')

        time.sleep(1)
        # go to checkout
        checkout_url = 'https://www.sierra.com/checkout/login/'
        driver.get(checkout_url)
        time.sleep(1)
        # login_on_checkout(driver, credentials)

        # print_t('Login succesfull. Proceeding to checkout...')
        print_t('Entering guest checkout...')
        guest_checkout_button = driver.find_element_by_xpath('//form[@action="/checkout/startguestcheckout/"]/button')
        guest_checkout_button.click()

        # checkout_button = driver.find_element_by_xpath('//form[@class="js-cart-form"]/button')
        # checkout_button.click()

        # Fill in shipping data
        print_t('Filling out shipping data')
        try:
            set_data_by_id(driver, 'Binding_Address_FirstName', shipping.firstname)
            set_data_by_id(driver, 'Binding_Address_LastName', shipping.lastName)
            set_data_by_id(driver, 'Binding_Address_Address1', shipping.address)
            set_data_by_id(driver, 'Binding_Address_City', shipping.city)
            set_data_by_id(driver, 'Binding_Address_UsaPostalCode', shipping.zipCode)

            state_select = Select(driver.find_element_by_id('Binding_Address_UsaProvince'))
            state_select.select_by_value(shipping.state)

            set_data_by_xpath(driver, '//input[@name="Binding.Address.PhoneNumber"]', shipping.phoneNumber)
            set_data_by_xpath(driver, '//input[@name="Binding.EmailAddress"]', shipping.email)
            time.sleep(1)

            # Select shipping method
            driver.execute_script(
                'document.getElementById("shippingFormNewAddressShippingMethodId202").checked = true;')
            time.sleep(1)

            # Go to payment
            driver.find_element_by_xpath('//*[@id="shippingFormNewAddress"]/button').click()
        except IndexError:
            # Shipping address already set, skipping
            pass

        # Fill in payment data
        print_t('Fill in payment data')
        payment_iframe = driver.find_element_by_id('stpiframecc-normal')
        driver.switch_to.frame(payment_iframe)

        driver.find_element_by_xpath('//div[@class="ccNumber"]/input').send_keys(payment.creditcard)
        driver.find_element_by_xpath('//div[@class="ccSecurityCode"]/input').send_keys(payment.cvv)

        month_select = Select(driver.find_element_by_id('expMonth'))
        year_select = Select(driver.find_element_by_id('expYear'))
        month_select.select_by_value(payment.experationMonth)
        year_select.select_by_value(payment.experationYear)

        driver.switch_to.default_content()

        # Check "Billing address" checkbox
        # checkbox_element = driver.find_element_by_id('BillToIsShipTo')
        # driver.execute_script("arguments[0].click();", checkbox_element)
        # time.sleep(1)

        find_click_button(driver, 'submitPayment')
        time.sleep(1)

        # ReCaptcha check
        recaptcha_element = None
        print_t('Looking for recaptcha...')
        try:
            recaptcha_element = driver.find_element_by_class_name('g-recaptchaOuter')

        except NoSuchElementException:
            # no recaptcha
            print_t('No recaptcha')
            pass

        if recaptcha_element:
            print_t("Recaptcha found, bypassing")
            iframe_text = recaptcha_element.find_elements_by_tag_name('noscript')[0].get_attribute('innerHTML')
            google_key_regex = r'(?<=k=).\w+'
            google_key = re.findall(google_key_regex, iframe_text)[0]
            print_t("Solving captcha...")
            start_time = time.time()
            captcha_result = solver.recaptcha(sitekey=google_key, url=driver.current_url)
            print_t(f'Captcha solved. Time elapsed: {round(time.time() - start_time, 2)} seconds.')
            driver.execute_script('arguments[0].parentNode.removeChild(arguments[0]);', recaptcha_element)

            form_element = driver.find_element_by_xpath('//form[@action="/checkout/review/"]')

            exec_script = f'''
            let captcha_response = document.createElement('textArea');
            captcha_response.setAttribute("name", "g-recaptcha-response");
            captcha_response.value = "{captcha_result["code"]}";
            let form_element = arguments[0];
            form_element.appendChild(captcha_response);
            '''

            driver.execute_script(exec_script, form_element)
            print_t('Captcha bypassed, placing order...')

        time.sleep(1)
        if TEST:
            print_t('Captcha bypassed, you can now close bot or place order to ensure it\'s working')
            time.sleep(300)
        else:
            place_order_button = driver.find_element_by_xpath('//form[@action="/checkout/review/"]/button')
            place_order_button.click()
    except KeyboardInterrupt:
        print_t('CTRL+C, exiting...')
        driver.quit()
        sys.exit(0)
    except Exception as e:
        if driver:
            screenshot_file_name = f'screenshots/webdriver_exception_{int(time.time())}.png'
            print_t(f'Webdriver exception, screenshot at {screenshot_file_name}')
            driver.save_screenshot(screenshot_file_name)
            driver.quit()
        raise e


def add_to_cart(driver, product_url, product_quantity):
    driver.get(product_url)

    try:
        driver.find_element_by_xpath('//h1[contains(text(), "Access Denied")]')
        print_t(f'"Access denied" page returned, sleeping for {REQUESTS_DELAY*5}s...')
        time.sleep(REQUESTS_DELAY*5)
        return False
    except NoSuchElementException:
        pass

    # Workaround to close popup
    try:
        WebDriverWait(driver, DEFAULT_DRIVER_WAIT).until(EC.presence_of_element_located((By.ID, 'dfSignup')))
        try:
            driver.find_element_by_id('lightBoxClose').click()
        except:
            pass
    except TimeoutException:
        # No popup, skipping
        pass

    # Setting quantity value via Javascript to avoid input field clearing
    quantity_input = driver.find_element_by_id('quantity')
    driver.execute_script(f'arguments[0].value = "{product_quantity}";', quantity_input)
    find_click_button(driver, 'addToCartButton')

    # Wait for confirmation frame
    WebDriverWait(driver, DEFAULT_DRIVER_WAIT).until(EC.presence_of_element_located((By.ID, 'quickViewIframe')))

    # Checking if item has been added to cart
    popup_frame = driver.find_element_by_id('quickViewIframe')
    driver.switch_to.frame(popup_frame)
    try:
        driver.find_element_by_xpath('//*[contains(text(), "Successfully added to your cart!")]')
        has_been_added = True
    except NoSuchElementException:
        has_been_added = False
        try:
            driver.find_element_by_xpath(
                '//*[contains(text(), "There was an issue updating your cart with the requested quantity")]')

            print_t('"There was an issue updating your cart with the requested quantity": not in stock yet')
        except NoSuchElementException:
            pass

    return has_been_added


def main(target_product, product_quantity):
    print_t("Started")
    urls = get_matched_and_available(target_product)
    print_t(f'Found {len(urls)} matches.')
    if len(urls) == 0:
        print_t('No match found - checking again')
        return
    print_t(f'Processing first url: {urls[0]}')
    # just buy the first match
    url = urls[0]
    for i in range(5):
        try:
            print_t(f'Attempt #{i + 1}')
            perform_purchase(url, product_quantity)
            print_t('Done.')
            return True
        except KeyboardInterrupt:
            print_t('CTRL+C, exiting...')
            sys.exit(0)
        except:
            print_t('Retrying perform_purchase()...')
            print_t('------------------STACK TRACE------------------')
            print_t(traceback.format_exc())
            print_t('----------------END STACK TRACE----------------')


# define main
if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Supremebot main parser')
    parser.add_argument('-n', '--name', required=True,
                        help='Specify product name to find and purchase')
    parser.add_argument('-q', '--quantity', required=True)
    parser.add_argument('-t', '--test', required=False, action='store_true')
    parser.add_argument('-s', '--sessions', required=False, default=50)
    parser.add_argument('-d', '--delay', required=False, default=1)
    parser.add_argument('-w', '--wait', required=False, default=5)

    args = parser.parse_args()

    REQUESTS_DELAY = args.delay
    SESSIONS_COUNT = args.sessions
    DEFAULT_DRIVER_WAIT = args.wait

    result = False
    attempts = 1
    if args.test:
        TEST = True

    current_time = datetime.datetime.now()
    time_str = current_time.strftime('%d_%m_%g_%H_%M')

    if not os.path.exists('screenshots'):
        os.makedirs('screenshots')

    if not os.path.exists('logs'):
        os.makedirs('logs')

    log_file_name = str_to_file_name(f'logs/{args.name.split("/")[-1]}_{time_str}.log')
    logging.basicConfig(filename=log_file_name, level=logging.INFO, format='[%(asctime)s][%(levelname)s] %(message)s')

    if TEST:
        print_t('-------------------TEST RUN--------------------')

    while not result:
        print_t(f'main(), attempt #{attempts}')
        try:
            result = main(target_product=args.name, product_quantity=args.quantity)
        except KeyboardInterrupt:
            print_t('CTRL+C, exiting...')
            sys.exit(0)
        except Exception as ex:
            print_t('Retrying main()')
            print_t('------------------STACK TRACE------------------')
            print_t(traceback.format_exc())
            print_t('----------------END STACK TRACE----------------')
            attempts += 1
